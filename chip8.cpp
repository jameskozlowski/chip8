#include "chip8.h"

Chip8::Chip8()
{
	for(int i = 0; i < 4096; ++i)
		WriteMemory(i, 0);
	srand(time(0));
}

Chip8::~Chip8()
{
}

/*
 * @Disc Initializes the game, loads fonts
 * @Return Returns true if successfull
*/
void Chip8::Initialize()
{
	pc = 0x200;  				// Program counter starts at 0x200
	sp = 0;
	I = 0;
	timerDelay = 0;
	timerSound = 0;
	refreshScreen = false;
	playBeep = false;
	for(int i = 0; i < 16; ++i)
		V[i] = 0;
	for(int i = 0; i < 16; ++i)
		key[i] = 0;		
	for (int i = 0; i < 64 * 32; ++i)
		videoMemory[i] = 0;
	refreshScreen = false;
	for(int i = 0; i < 80; ++i)
		memory[i] = chip8_fontset[i];
	lastTick = getMilliCount();
	lastTick2 = getMilliCount();
	
}

/*
 * @Disc Loads a game file into memory
 * @Param filename name of file to load
 * @Return Returns true if file was loaded
*/
bool Chip8::LoadFile(char* filename)
{
	ifstream myFile;
    myFile.open (filename, ios::in | ios::binary);
	
	myFile.seekg(0, std::ifstream::end);
	int size = myFile.tellg();
	myFile.seekg(0);
	myFile.read ((char*)&memory[0x200], size);
	
	myFile.close();
}

/*
 * @Disc Writes a byte to memory
 * @param location location in memory to write to
 * $param value value to write to memory
 * @return Void
*/
void Chip8::WriteMemory(unsigned short location, unsigned char value)
{
	memory[location] = value;
}

/*
 * @Disc Reads a byte of memory
 * @param location location in memory to read
 * @return value from memory location
*/
unsigned char Chip8::ReadMemory(unsigned short location)
{
	return memory[location];
}

/*
 * @Disc Gets time as mS
 * @return time as MS
*/
unsigned long Chip8::getMilliCount()
{
	timeb tb;
	ftime(&tb);
	int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
	return nCount;
}

/*
 * @Disc returns time span from nTimeStart
 * @param nTimeStart -Time to measure span from
 * @return time span
*/
unsigned long  Chip8::getMilliSpan(unsigned long nTimeStart)
{
	int nSpan = getMilliCount() - nTimeStart;
	if(nSpan < 0)
		nSpan += 0x100000 * 1000;
	return nSpan;
}

/*
 * @Disc Save game state
 * @param filename to save as
 * @return void
*/
void Chip8::SaveState(char *filename)
{
	ofstream myFile;
    myFile.open (filename, ios::out | ios::binary);
	
	
	myFile.write ((char*)&pc, sizeof (pc));
	myFile.write ((char*)&I, sizeof (I));
	myFile.write ((char*)&V, sizeof (V));
	myFile.write ((char*)&timerDelay, sizeof (timerDelay));
	myFile.write ((char*)&timerSound, sizeof (timerSound));
	myFile.write ((char*)&stack, sizeof (stack));
	myFile.write ((char*)&sp, sizeof (sp));
	myFile.write ((char*)&videoMemory, sizeof (videoMemory));
	myFile.write ((char*)&memory, sizeof (memory));
	
	myFile.close();
}

/*
 * @Disc Load game state
 * #param filename to load
 * @return void
*/
void Chip8::LoadState(char *filename)
{
	ifstream myFile;
    myFile.open (filename, ios::in | ios::binary);
	
	
	myFile.read ((char*)&pc, sizeof (pc));
	myFile.read ((char*)&I, sizeof (I));
	myFile.read ((char*)&V, sizeof (V));
	myFile.read ((char*)&timerDelay, sizeof (timerDelay));
	myFile.read ((char*)&timerSound, sizeof (timerSound));
	myFile.read ((char*)&stack, sizeof (stack));
	myFile.read ((char*)&sp, sizeof (sp));
	myFile.read ((char*)&videoMemory, sizeof (videoMemory));
	myFile.read ((char*)&memory, sizeof (memory));
	
	myFile.close();
}


/*
 * @Disc Emulates a clock cycle
 * @return Void
*/
void Chip8::emulateCycle()
{
	if (getMilliSpan(lastTick) < 1)
		return;
	lastTick = getMilliCount();

	unsigned short opcode = memory[pc] << 8 | memory[pc + 1];
	//printf ("opcode [0x0000]: 0x%X\n", opcode);   
	switch(opcode & 0xF000)
	{    
		case 0x0000:
		switch(opcode & 0x000F)
		{
			case 0x0000: // 0x00E0: Clears the screen        
				for (int i = 0; i < 64 * 32; ++i)
					videoMemory[i] = 0;
				refreshScreen = true;
				pc += 2;
			break;

 
			case 0x000E: // 0x00EE: Returns from subroutine          
			pc = stack[sp-1];
			sp--;
			pc += 2;
			break;
 
			default:
				printf ("Unknown opcode [0x0000]: 0x%X\n", opcode);          
		}
		break;		
		case 0x1000: // 1NNN: Jups to address
			pc = opcode & 0x0FFF;
			
		break;
		
		case 0x2000: // 2NNN 	Calls subroutine at NNN.
			stack[sp] = pc;
			++sp;
			pc = opcode & 0x0FFF;
		break;
		case 0x3000: // 3XNN 	Skips the next instruction if VX equals NN.
			if (V[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF) )
				pc += 4;
			else
				pc += 2;
		break;
		case 0x4000: // 4XNN 	Skips the next instruction if VX doesn't equal NN.
			if (V[(opcode & 0x0F00) >> 8] != (opcode & 0x00FF) )
				pc += 4;
			else
				pc += 2;
		break;
		case 0x5000: // 5XY0 	Skips the next instruction if VX equals VY.
			if (V[(opcode & 0x0F00) >> 8] == V[(opcode & 0x00F0) >> 4] )  
				pc += 4;
			else
				pc += 2;
		break;
		case 0x6000: // 6XNN 	Sets VX to NN.
			V[(opcode & 0x0F00) >> 8] = (opcode & 0x00FF);
			pc += 2;
		break;
		case 0x7000: // 7XNN 	Adds NN to VX.
			V[(opcode & 0x0F00) >> 8] += (opcode & 0x00FF);
			pc += 2;
		break;
		case 0x8000: 
				switch(opcode & 0x000F)
				{
					case 0x0000: // 8XY0 	Sets VX to the value of VY.
						V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4];  
						pc += 2;
					break;
					case 0x0001: // 8XY1    Sets VX to VX or VY.
						V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x0F00) >> 8] | V[(opcode & 0x00F0) >> 4];  
						pc += 2;
					break;
					case 0x0002: // 8XY2    Sets VX to VX and VY.
						V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x0F00) >> 8] & V[(opcode & 0x00F0) >> 4]; 
						pc += 2;
					break;
					case 0x0003: // 8XY1    Sets VX to VX xor VY.
						V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x0F00) >> 8] ^ V[(opcode & 0x00F0) >> 4]; 
						pc += 2;
					break;
					case 0x0004: // 8XY4 	Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't.       
						if(V[(opcode & 0x00F0) >> 4] > (0xFF - V[(opcode & 0x0F00) >> 8]))
							V[0xF] = 1; //carry
						else
							V[0xF] = 0;
						V[(opcode & 0x0F00) >> 8] += V[(opcode & 0x00F0) >> 4];
						pc += 2;          
					break;
					case 0x0005: // 8XY5 	VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't.  
						if(V[(opcode & 0x00F0) >> 4] > V[(opcode & 0x0F00) >> 8]) 
							V[0xF] = 0; // there is a borrow
						else 
							V[0xF] = 1;					
						V[(opcode & 0x0F00) >> 8] -= V[(opcode & 0x00F0) >> 4];
						pc += 2;     
					break;
					case 0x0006: // 8XY6 	Shifts VX right by one. VF is set to the value of the least significant bit of VX before the shift.
						V[0xF] = (V[(opcode & 0x0F00) >> 8]) & 0x1;
						V[(opcode & 0x0F00) >> 8] >>= 1;
						pc += 2;
					break;
					case 0x0007: // 8XY7 	Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
						if(V[(opcode & 0x0F00) >> 8] > V[(opcode & 0x00F0) >> 4])	// VY-VX
							V[0xF] = 0; // there is a borrow
						else
							V[0xF] = 1;
						V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4] - V[(opcode & 0x0F00) >> 8];				
						pc += 2;
					break;
					case 0x000E: // 8XYE 	Shifts VX left by one. VF is set to the value of the most significant bit of VX before the shift.
						V[0xF] = V[(opcode & 0x0F00) >> 8] >> 7;
						V[(opcode & 0x0F00) >> 8] <<= 1;
						pc += 2;
					break;
					default:
						printf ("Unknown opcode [0x0000]: 0x%X\n", opcode);         
				}
		break;
		
		case 0x9000: // 9XY0 	Skips the next instruction if VX doesn't equal VY.
			if (V[(opcode & 0x0F00) >> 8] != V[(opcode & 0x00F0) >> 4])
				pc += 4;
			else
				pc += 2;
		break;
		
		case 0xA000: // ANNN: Sets I to the address NNN
			I = opcode & 0x0FFF;
			pc += 2;
		break;
		
		case 0xB000: // BNNN 	Jumps to the address NNN plus V0.
			pc = (opcode & 0x0FFF) + V[0];
		break;
		
		case 0xC000: // CXNN 	Sets VX to a random number and NN.
			V[(opcode & 0x0F00) >> 8] = (rand() % 0xFF) & (opcode & 0x00FF);
			pc += 2;
		break;		
		
		case 0xD000: // DXYN 	Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N pixels. 
		{
			unsigned short x = V[(opcode & 0x0F00) >> 8];
			unsigned short y = V[(opcode & 0x00F0) >> 4];
			unsigned short height = opcode & 0x000F;
			unsigned short pixel;

			V[0xF] = 0;
			for (int yline = 0; yline < height; yline++)
			{
				pixel = memory[I + yline];
				for(int xline = 0; xline < 8; xline++)
				{
					if((pixel & (0x80 >> xline)) != 0)
					{
						if(videoMemory[(x + xline + ((y + yline) * 64))] == 1)
						{
							V[0xF] = 1;                                    
						}
						videoMemory[x + xline + ((y + yline) * 64)] ^= 1;
					}
				}
			}
			refreshScreen = true;
			pc += 2;			
		}
		break;
		case 0xE000:
			switch(opcode & 0x00FF)
			{
				case 0x009E: // EX9E 	Skips the next instruction if the key stored in VX is pressed.
					if(key[V[(opcode & 0x0F00) >> 8]] != 0)
						pc += 4;
					else
						pc += 2;
				break;
				case 0x0A1: // EXA1 	Skips the next instruction if the key stored in VX isn't pressed.
					if(key[V[(opcode & 0x0F00) >> 8]] == 0)
						pc += 4;
					else
						pc += 2;
				break;
				default:
					printf ("Unknown opcode [0x0000]: 0x%X\n", opcode);  	
			}		
		break;
		
		case 0xF000:
			switch(opcode & 0x00FF)
			{
				case 0x0007: //FX07 	Sets VX to the value of the delay timer.
					V[(opcode & 0x0F00) >> 8] = timerDelay;
					pc += 2;
				break;
				
				case 0x000A: // FX0A 	A key press is awaited, and then stored in VX.
				{
					bool keyPress = false;

					for(int i = 0; i < 16; ++i)
					{
						if(key[i] != 0)
						{
							V[(opcode & 0x0F00) >> 8] = i;
							keyPress = true;
						}
					}
					// If we didn't received a keypress, skip this cycle and try again.
					if(!keyPress)						
						return;

					pc += 2;
				}				
				break;
				
				case 0x0015: // FX15 	Sets the delay timer to VX.
					timerDelay = V[(opcode & 0x0F00) >> 8];
					pc += 2;
				break;
				
				case 0x0018: // FX18 	Sets the sound timer to VX.
					timerSound = V[(opcode & 0x0F00) >> 8];
					pc += 2;
				break;
				
				case 0x001E: // FX1E 	Adds VX to I.
					if(I + V[(opcode & 0x0F00) >> 8] > 0xFFF)	// VF is set to 1 when range overflow (I+VX>0xFFF), and 0 when there isn't.
						V[0xF] = 1;
					else
						V[0xF] = 0;
					I += V[(opcode & 0x0F00) >> 8];
					pc += 2;
				break;
				
				case 0x0029: // FX29 	Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font.
					I = V[(opcode & 0x0F00) >> 8] * 0x5;
					pc += 2;
				break;
				
				case 0x0033: // FX33 	Stores the Binary-coded decimal representation of VX, with the most significant of three digits at the address in I, the middle digit at I plus 1, and the least significant digit at I plus 2. 
					memory[I]     = V[(opcode & 0x0F00) >> 8] / 100;
					memory[I + 1] = (V[(opcode & 0x0F00) >> 8] / 10) % 10;
					memory[I + 2] = (V[(opcode & 0x0F00) >> 8] % 100) % 10;
					pc += 2;
				break;
				
				case 0x0055: // FX55 	Stores V0 to VX in memory starting at address I.
					for (int i = 0; i <= (opcode & 0x0F00) >> 8; ++i)
						memory[I + i] = V[i];
					I += ((opcode & 0x0F00) >> 8) + 1;
					pc += 2;
				break;
				
				case 0x0065: // FX65 	Fills V0 to VX with values from memory starting at address I.
					for (int i = 0; i <= (opcode & 0x0F00) >> 8; ++i)
						V[i] = memory[I + i];
					I += ((opcode & 0x0F00) >> 8) + 1;
					pc += 2;
				break;
				
				default:
					printf ("Unknown opcode [0x0000]: 0x%X\n", opcode);  
			}


		break;

		default:
			printf ("Unknown opcode: 0x%X\n", opcode);
	}  
	
	if (getMilliSpan(lastTick2) > 6)
	{
		lastTick2 = getMilliCount();
	if(timerDelay > 0)
		--timerDelay;
 
	if(timerSound > 0)
	{
		if(timerSound == 1)
			playBeep = true;
		--timerSound;
	}
	
	}  
}