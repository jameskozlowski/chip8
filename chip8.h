#ifndef CHIP8_H
#define CHIP8_H

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <sys/timeb.h>

using namespace std;

#define COMBINE(msb,lsb) (msb << 8 ) | (lsb)										//combines to bytes into a word

class Chip8
{
public:
	bool refreshScreen;
	bool playBeep;
	unsigned char videoMemory[64 * 32];												//Video Memory	unsigned char videoMemory[64 * 32];												//Video Memory
	unsigned char key[16];															//Key pressed?
	Chip8();
	~Chip8();
	bool LoadFile(char* filename);													//Loads a game file
	void Initialize();																//Initializes the game
	void emulateCycle();															//Emulates a clock cycle
	void SaveState(char *filename);													//Saves Game State
	void LoadState(char *filename);													//Loads Game state

private:
	unsigned char memory[4096];														//Memory

	unsigned short pc;																//memory pointer
	unsigned short I;																//16 bit I register
	unsigned char V[16];															//8 bit V registers
	unsigned char timerDelay;														//Delay Timer
	unsigned char timerSound;														//Sound Timer
	unsigned short stack[16];														//Stack register
	unsigned int sp;																//Stack Pointer
	unsigned long lastTick;
	unsigned long lastTick2;
	
	unsigned char chip8_fontset[80] =												//Default Fonts
	{ 
		0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
		0x20, 0x60, 0x20, 0x20, 0x70, // 1
		0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
		0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
		0x90, 0x90, 0xF0, 0x10, 0x10, // 4
		0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
		0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
		0xF0, 0x10, 0x20, 0x40, 0x40, // 7
		0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
		0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
		0xF0, 0x90, 0xF0, 0x90, 0x90, // A
		0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
		0xF0, 0x80, 0x80, 0x80, 0xF0, // C
		0xE0, 0x90, 0x90, 0x90, 0xE0, // D
		0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
		0xF0, 0x80, 0xF0, 0x80, 0x80  // F
	};
	
	void WriteMemory(unsigned short location, unsigned char value);					//Write a byte to memory
	unsigned char ReadMemory(unsigned short location);								//Read a byte from memory
	int Random(int max) { return static_cast<double>(rand()) / max * 14620 + 1; }   //returns a random number   
	unsigned long getMilliCount();
	unsigned long getMilliSpan(unsigned long nTimeStart);
	
};

#endif // CHIP8_H
